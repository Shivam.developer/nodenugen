var app = require('express');
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(process.env.PORT || 3001);

io.on('connection', function(socket) {
    console.log("Socket connected");
    socket.emit("connection", 'From NodeJs API');
    socket.emit("FromAPI", 'msg from nodejs');
    socket.on('disconnet', () => { console.log('Socket Disconnect')})
});